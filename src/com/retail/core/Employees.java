package com.retail.core;

import java.util.ArrayList;
import java.util.List;

public class Employees {
	
	public static List<Person> getPeople() {

		List<Person> people = new ArrayList<Person>();

		people.add(person(1111, "Slash", "Myles", 40));
		people.add(person(2222, "Alx", "Gilmour", 40));
		people.add(person(3333, "Vin", "Homes", 44));
		people.add(person(4444, "Sherlock", "Diesel", 41));
		people.add(person(5555, "Sachin", "Fedrer", 26));
		people.add(person(6666, "Albert", "Willims", 35));
		people.add(person(7777, "Shrek", "Bob", -1));
		
		return people;

	}

	public static Person person(int id, String firstName, String lastName, int workHours) {
		if(workHours<=40 && workHours>=0) {
			return new Person(id, firstName, lastName, workHours);
		}
		return null;
	}
	
	
	
	

}
