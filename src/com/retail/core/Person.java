package com.retail.core;

public class Person {
	
	private int id;
	private String firstName;
	private String lastName;
	private int workHours;
	
	public Person(int id, String firstName, String lastName, int workHours) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.workHours = workHours;
	}

	public int getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public int getWorkHours() {
		return workHours;
	}

	@Override
	public String toString() {
		if(getWorkHours()<=40 && getWorkHours()>=0) {
			return getId()+" "+getFirstName()+" "+getLastName()+" "+getWorkHours();
		}
		return " ";
	}
	
	
	

}
