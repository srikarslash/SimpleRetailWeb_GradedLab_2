package com.retail.web;

import static org.junit.Assert.*;

import org.junit.Test;

import com.retail.core.Employees;
import com.retail.core.Person;

import cucumber.api.java.Before;

public class EmployeeTest {
	
	Employees x;
	
	@Before
	public void setUp() {
		x = new Employees();
	}
	
	@Test
	public void personWithLowTest() {
		
		Person actual = x.person(1, "Slash", "Myles", 0);
		assertEquals("1 Slash Myles 0", actual.toString());
	}

	@Test
	public void personWithMedTest() {
		
		Person actual = x.person(1, "Slash", "Myles", 10);
		assertEquals("1 Slash Myles 10", actual.toString());
	}
	
	@Test
	public void personWithHighTest() {
		
		Person actual = x.person(1, "Slash", "Myles", 40);
		assertEquals("1 Slash Myles 40", actual.toString());
	}
	
	@Test
	public void personWithExtraHighTest() {
		
		Person actual = x.person(1, "Slash", "Myles", 41);
		assertEquals(null, actual);
	}
	
	@Test
	public void personWithExtraLowTest() {
		
		Person actual = x.person(1, "Slash", "Myles", -1);
		assertEquals(null, actual);
	}

}
